import { login, getInfo } from '@/api/user'
import { setToken, getToken, removeToken } from '@/utils/auth'
import { constantRoutes, resetRouter } from '@/router'
export default {
  // 开启命名空间
  namespaced: true,
  // 放数据的地方
  state: () => ({
    token: getToken() || '',
    userinfo: {},
    routes: constantRoutes
  }),
  // 相当于组件中计算属性
  getters: {},
  // 这个是唯一修改state中数据地方
  mutations: {
    setToken(state, payload) {
      state.token = payload
      setToken(payload)
    },
    setUserInfo(state, payload) {
      state.userinfo = payload
    },
    updateRoutes(state, payload) {
      state.routes = [...constantRoutes, ...payload]
    }
  },
  // 写异步的ajax的地方
  actions: {
    async login(context, payload) {
      console.log(payload)
      const res = await login(payload)
      console.log(res)
      context.commit('setToken', res)
    },
    async getInfo(context, payload) {
      const res = await getInfo()
      console.log(res)
      context.commit('setUserInfo', res)
      return res
    },
    // 这个actions没有异步，但是还是有一个好处，把二个mutations封装起来 加removeToken
    logout(context) {
      // 清除token （state 本地存储）
      // 清除userInfo
      context.commit('setToken', '')
      removeToken()
      context.commit('setUserInfo', {})
      resetRouter()// 重置路由
    }
  }
}
