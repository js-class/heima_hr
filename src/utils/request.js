import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
// console.log(process.env.VUE_APP_BASE_API)// 基地址
// console.log(process.env.NODE_ENV)// development
const service = axios.create({
  // baseURL: 'http://localhost:9528/abc/api',
  // /api是开发的时候测试服务器地址 /prod-api 线上真实服务器地址
  // baseURL: '/abc/api',
  // baseURL: '/abc/prod-api',
  baseURL: '/abc' + process.env.VUE_APP_BASE_API,
  timeout: 10000
})
// 添加请求拦截器
service.interceptors.request.use(function(config) {
  const token = store.getters.token
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  // 在发送请求之前做些什么
  return config
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function(response) {
  console.log(response)
  // 如果发现返回的数据类型是blob，直接就return response.data 就不走下面
  if (response.data instanceof Blob) {
    return response.data
  }
  // 由于每个接口都是有sucess,message,data所以我们为了使用方式，直接解构
  const { success, message, data } = response.data
  // 如果success的值是false就代表的是登录失败了，我们就直接用Promise.reject来让代码拒绝掉
  if (success === false) {
    Message.error(message)
    // 我们需要在成功的时候让代码执行逻辑去失败中
    return Promise.reject(new Error(message))
  }
  // 对响应数据做点什么
  return data
}, function(error) {
  console.dir(error)
  if (error.response.status === 401) {
    Message.error(error.response?.data?.message)// 可选链 防止前面已是空值 后面再打点就会报错 加了？可选链如果前面是空值就直接返回
    // 清token 清用户信息 $router.push
    store.dispatch('user/logout')
    router.push('/login')
  }

  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error)
})
export default service
