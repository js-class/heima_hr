import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/sys/profile',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

export const modifyPass = data => {
  return request({
    method: 'PUT',
    url: '/sys/user/updatePass',
    data
  })
}

// 获取员工列表
export const getUsers = params => {
  return request({
    url: '/sys/user',
    params
  })
}

// 批量导出员工列表
// axios默认返回的数据类型是json,如果返回的不是json 需要告诉axios最终数据格式是什么样子
// 由于这个接口返回的是二进制，所以要设置responseType
export const exportExcel = () => {
  return request({
    url: '/sys/user/export',
    responseType: 'blob'
  })
}

// 下载模板
export const downloadTemplate = () => {
  return request({
    url: '/sys/user/import/template',
    responseType: 'blob'
  })
}

// 导入excel接口
// new FormData
export const importExcel = data => {
  return request({
    method: 'POST',
    url: '/sys/user/import',
    data
  })
}

// 获取员工详情
export const getUserDetail = id => {
  return request({
    url: `/sys/user/${id}`
  })
}

// 新增员工
export const addUser = data => {
  return request({
    method: 'POST',
    url: '/sys/user',
    data
  })
}
// 修改员工信息
export const editUser = data => {
  return request({
    method: 'PUT',
    url: `/sys/user/${data.id}`,
    data
  })
}

// 为员工分配角色
export const assignRole = data => {
  return request({
    method: 'PUT',
    url: '/sys/user/assignRoles',
    data
  })
}
