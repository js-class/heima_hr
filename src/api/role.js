// 角色相关的接口
import request from '@/utils/request'
// 获取所有的角色
export const getRoles = params => {
  return request({
    url: '/sys/role',
    params
  })
}

// 删除角色
export const delRole = id => {
  return request({
    method: 'DELETE',
    url: `/sys/role/${id}`
  })
}

// 新增角色
export const addRole = data => {
  return request({
    method: 'POST',
    url: '/sys/role',
    data
  })
}

// 获取角色详情
export const getRole = id => {
  return request({
    url: `/sys/role/${id}`
  })
}

// 给角色分配权限
export const assignPermission = data => {
  return request({
    url: '/sys/role/assignPrem',
    method: 'PUT',
    data
  })
}

// 修改角色
export const editRole = data => {
  return request({
    method: 'PUT',
    url: `/sys/role/${data.id}`,
    data
  })
}
