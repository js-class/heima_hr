import request from '@/utils/request'
export const getDepartments = () => {
  return request({
    url: '/company/department'
  })
}

// 删除部门
export const delDepartment = id => {
  return request({
    method: 'DELETE',
    url: `/company/department/${id}`
  })
}

// 获取部门负责人列表
export const getDepartmentsManager = () => {
  return request({
    url: '/sys/user/simple'
  })
}

// 新增子部门
export const addDepartment = data => {
  return request({
    method: 'POST',
    url: '/company/department',
    data
  })
}

// 获取部门详情
export const getDepartment = id => {
  return request({
    url: `/company/department/${id}`
  })
}

// 保存修改的结果
export const editDepartment = data => {
  return request({
    method: 'PUT',
    url: `/company/department/${data.id}`,
    data
  })
}
