import request from '@/utils/request'
// 获取所有的权限点
export const getPermissions = () => {
  return request({
    url: '/sys/permission'
  })
}
