import router, { asyncRoutes } from '@/router'
import store from '@/store'
import defaultSettings from '@/settings'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
const whiteList = ['/login', '/404']
// 前置路由守卫
// 我现在在/dashboard 然后我想访问 /login 就会进入 beforeEach 就会开启进度效果 但是我到了beforeEach之后被打回来了
// 就会导致afterEach没有机会执行
router.beforeEach(async(to, from, next) => {
  NProgress.start()// 开启进度
  // store.state.user.token //一种写法
  const token = store.getters.token
  if (token) {
    // 如果登录过了，就不允许再去/login
    // 为了避免重复登录
    if (to.path === '/login') {
      next('/')
    } else {
      // 如果userInfo拿了，就不用重复发请求
      if (!store.getters.name) {
        const userInfo = await store.dispatch('user/getInfo')
        console.log(userInfo.roles.menus)// 权限点数组
        console.log(asyncRoutes)// 动态路由
        // 默认的asyncRoutes是一个完整的动态路由的数组
        // menus是当前这个用户所拥有的权限点，我们就筛选完整的动态路由数组
        // 得到arr就是当前这个用户拥有的动态路由
        const filterRoutes = asyncRoutes.filter(item => userInfo.roles.menus.includes(item.name))
        store.commit('user/updateRoutes', filterRoutes)
        // 如何给现有的路由添加新的路由 默认只有静态路由
        router.addRoutes([...filterRoutes, { path: '*', redirect: '/404', hidden: true }])
        next(to.path)// 回去一趟才能有完整最新的路由信息
      }
      next()
    }
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
    }
  }
  NProgress.done()
})
// 后置路由守卫
// 路由规则
router.afterEach((to) => {
  NProgress.done()// 结束进度
  // 如果当前这个路由规则没有title就用默认的title,如果有，就用 默认title-当前title
  if (to.meta.title) {
    document.title = defaultSettings.title + '-' + to.meta.title
  } else {
    document.title = defaultSettings.title
  }
})

// import router from './router'
// import store from './store'
// import { Message } from 'element-ui'
// import NProgress from 'nprogress' // progress bar
// import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from '@/utils/auth' // get token from cookie
// import getPageTitle from '@/utils/get-page-title'

// NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const whiteList = ['/login'] // no redirect whitelist

// router.beforeEach(async(to, from, next) => {
//   // start progress bar
//   NProgress.start()

//   // set page title
//   document.title = getPageTitle(to.meta.title)

//   // determine whether the user has logged in
//   const hasToken = getToken()

//   if (hasToken) {
//     if (to.path === '/login') {
//       // if is logged in, redirect to the home page
//       next({ path: '/' })
//       NProgress.done()
//     } else {
//       const hasGetUserInfo = store.getters.name
//       if (hasGetUserInfo) {
//         next()
//       } else {
//         try {
//           // get user info
//           await store.dispatch('user/getInfo')

//           next()
//         } catch (error) {
//           // remove token and go to login page to re-login
//           await store.dispatch('user/resetToken')
//           Message.error(error || 'Has Error')
//           next(`/login?redirect=${to.path}`)
//           NProgress.done()
//         }
//       }
//     }
//   } else {
//     /* has no token*/

//     if (whiteList.indexOf(to.path) !== -1) {
//       // in the free login whitelist, go directly
//       next()
//     } else {
//       // other pages that do not have permission to access are redirected to the login page.
//       next(`/login?redirect=${to.path}`)
//       NProgress.done()
//     }
//   }
// })

// router.afterEach(() => {
//   // finish progress bar
//   NProgress.done()
// })

// store.state.user.userinfo.username
// store.getters.username

// function fn(){
//   async function fn1(){
//     await
//   }
// }

// 动态路由的思路
// 1. 路由规则分为两部分，一部分是静态路由(constantRoutes)，也就是不管是哪个账号都有的通用路由，一种是动态路由(asyncRoutes)，这个是需要根据当前用户的权限点来筛选出来的
// 2. 我们用户在登录成功的时候，就会去获取用户基本信息，在里面有一个叫menus的数组，记录了当前这个用户有哪些权限
// 3. 我们把asyncRoutes根据menus进行筛选得到filterRoutes，然后使用router.addRoutes把filterRoutes添加到路由规则中

// next(to.path) 因为路由对象当前只有旧的路由规则，必须先回去一下才能拿到完整
// next(to.path)和next()共同点都是去同一个地方 区别next(to.path要先回去一下)

// router.addRoutes([...filterRoutes, { path: '*', redirect: '/404', hidden: true }]) 保证通配符路由规则在最后面

// 你现在访问 /login --> 到了路由守卫 next('/login')
