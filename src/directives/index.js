import Vue from 'vue'
import store from '@/store'
// v-permission
// <el-button v-permission="'user_delete'">删除</el-button>
Vue.directive('permission', {
  inserted(el, binding) {
    if (!store.state.user.userinfo?.roles?.points.includes(binding.value)) {
      el.remove()
    }
  }
})

// 面试问：你项目中的权限是怎么处理的
// 权限分为四种，分别是页面级别、菜单级别、按钮级别、接口api级别
// 1. 页面级别 用户在登录的时候就会获取用户基本信息，在里面有一个menus的权限数组，我们路由分为静态和动态的，我们动态是根据menus筛选出来的，并且通过addRoute和静态路由合并
// 2. 菜单级别 我们在实现页面权限的时候，就路由规则数组放到vuex中，菜单就是拿到这个路由数组，用v-for遍历出来的
// 3. 按钮级别 自定义v-permission来实现
// 4. 接口api级别 在请求拦截器中在headers中设置token,在响应拦截器如果发现响应状态码是401，就让用户重新登录
