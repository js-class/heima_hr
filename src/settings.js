module.exports = {

  title: '黑马人资', // 网站标题

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false, // 头部是否固定

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true// 侧边栏是否显示logo
}
