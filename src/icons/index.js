import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg component

// register globally
Vue.component('svg-icon', SvgIcon)

// 批量导入svg目录中所有的图标
// require.context有三个参数 第一个代表去哪里导图标 第二步 false 不去递归导 第三个其实是一个正则 代表只匹配以svg结尾
// require.context这个api是webpack提供的
const req = require.context('./svg', false, /\.svg$/)
// console.log(req.keys())
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)

// 100个图标，就要引100次
// import './svg/shopping.svg'

// 以前的时候我们需要用阿里图标库
// 上传之后
